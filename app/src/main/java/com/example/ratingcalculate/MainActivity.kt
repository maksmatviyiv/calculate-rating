package com.example.ratingcalculate

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {

    lateinit var calculateButton : Button
    lateinit var successRate : TextView
    lateinit var rating: TextView

    var courses1: List<String> = listOf(
        "test1",
        "test2",
        "test3",
        "test4",
        "test5",
        "test6",
        "test7"

    )
    var courses: List<String> = listOf(
        "Інтерфейси та протоколи передачі даних",
        "Основи цифрової обробки сигналів",
        "Технології захисту інформації",
        "Аналітичні та нереляційні бази даних",
        "Проектування Інтернет речей",
        "Документування програмного забезпечення та шаблони проектування",
        "Вибіркова дисципліна"
    )
    val credirs: List<String> = listOf("4", "5", "5", "5", "4", "4", "3")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rating_list.layoutManager = LinearLayoutManager(this)
        val numberCourse = intent.getStringExtra("NumberCourse")
        when(numberCourse) {
            "1" -> rating_list.adapter = RatingAdapter(firstCourseFirstSemester)
            "2" -> rating_list.adapter = RatingAdapter(firstCourseSecondSemester)
        }
        calculateButton = findViewById(R.id.calculate)
        rating = findViewById(R.id.rating_semester)
        successRate = findViewById(R.id.success_rate)

        calculateButton.setOnClickListener { buttonclick() }
    }

     private fun buttonclick() {
         var successRatingFinal : Double = 0.0
         var ratingFinal = 0.0F
         var calculateNumberOfGrade : Int = 0
         var sumCredits : Int = 0

         for (i in 0..credirs.size -1) {
             sumCredits = sumCredits + credirs[i].toInt()
             calculateNumberOfGrade = calculateNumberOfGrade + (credirs[i].toInt() * grades[i].toInt())

         }
         ratingFinal = (calculateNumberOfGrade.toFloat() / sumCredits.toFloat())
         successRatingFinal = 0.95 * ratingFinal
         successRate.text = successRatingFinal.toString()

         }







    }

