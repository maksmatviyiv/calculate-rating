package com.example.ratingcalculate


import android.content.ContentValues.TAG
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import android.content.Intent
import android.os.Bundle

var grades = mutableListOf<String>()
class RatingAdapter: RecyclerView.Adapter<RatingAdapter.ViewHolder> {
    var courseMap = hashMapOf<String, String>()
    var nameCourse = ArrayList(courseMap.keys)
    var credits = ArrayList(courseMap.values)



    constructor(courseMap: HashMap<String, String>) {
        this.courseMap = courseMap

    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatingAdapter.ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.rating_list_item, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: RatingAdapter.ViewHolder, position: Int) {

            holder.nameCourses.text = nameCourse[position]
            holder.credit.text = credits[position]

        holder.grade.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                var textfromEditText = holder.grade.text.toString()
                grades.add(position, textfromEditText)



            }

            override fun afterTextChanged(p0: Editable) {

            }
        })

        Log.d("Testing3", "$grades")
    }


    override fun getItemCount(): Int {
        return courseMap.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameCourses: TextView = itemView.findViewById(R.id.name_courses)
        val credit: TextView = itemView.findViewById(R.id.credits)
        val grade: EditText = itemView.findViewById(R.id.grade)
    }


}


