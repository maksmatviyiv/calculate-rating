package com.example.ratingcalculate

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

import androidx.appcompat.app.AppCompatActivity

class ChooseCourseActivity : AppCompatActivity() {
    lateinit var chooseCourseButton: Button
    lateinit var numberCourses: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_course)
        chooseCourseButton = findViewById(R.id.choose_courses)

        chooseCourseButton.setOnClickListener {
            numberCourses = findViewById(R.id.number_course)
            var stringNumberCourse = numberCourses.text.toString()
            var intent = Intent(this, MainActivity::class.java)
            intent.putExtra("NumberCourse", stringNumberCourse)
            startActivity(intent)
        }
    }
}